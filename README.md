# FLOW TRM 
NFT Marketplace developed on the FLOW Blockchain. This repository contains the custom smart contracts developed for the TRM project including:  
1. TrmAssetV1.cdc  
2. TrmRentV1.cdc  
3. TrmMarketV1.cdc   

## Asset
This is the main NFT Resource available on the Marketplace.

## Rent
This is a special Resource that gives a user the ability to access an Asset NFT for a limited period of time without owning the actual Asset NFT. 

## TrmMarket
This contract facilitates the listing/delisting of Assets, and executes the purchase and rent transactions.